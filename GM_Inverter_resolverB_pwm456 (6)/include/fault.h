/*****************************************************************************
-------------------------------Organization------------------------------------	
	Module Name 	: Fault.h
	Project		: Super High-Speed HYUNDAI Elevator(600m/min)
	Compiler    	: TMS320 Floating Point C-Compiler v5.51
	Author		: HELCO Super High Speed Team
	Version		: v1.0
	Last Rev.	: 2008.4.29
******************************************************************************/
#ifndef DSP2834x_FLT_
#define DSP2834x_FLT_

#ifdef __cplusplus
extern "C" {
#endif

#include "variable.h"
#include "DSP2834x_Device.h"



struct HW_BITS   {    // bits  description
//   Uint16 ShuntVolt		:1;     // 0     Hardware Shunt Resistor Voltage Fault
//   Uint16 ShuntCurrent	:1;     // 1     Hardware Shunt Sensing Current Fault
//   Uint16 Vdc			:1;     // 2     Hardware DC Link Voltage Fault
   Uint16 Current			:1;     // 2     Hardware DC Link Voltage Fault
   Uint16 I_u				:1;
   Uint16 I_v				:1;
   Uint16 I_w				:1;
   Uint16 Arm_short			:1;     // 2     Hardware DC Link Voltage Fault
   Uint16 Vdc				:1;     // 2     Hardware DC Link Voltage Fault
   Uint16 X_rsvd			:14;    // 15:3  reserved
};

union HW_REG {
   Uint16              all;
   struct HW_BITS  bit;
};

struct SW_BITS   {    // bits  description
   Uint16 I_u				:1;     // 0     Software a Phase Current Fault
   Uint16 I_v				:1;     // 1     Software b Phase Current Fault
   Uint16 I_w				:1;     // 2     Software c Phase Current Fault
   Uint16 I_dc				:1;     // 3     Software w Phase Current Fault
   Uint16 I_b			:1;     // 4     Software DC Link Voltage Over Fault
   Uint16 I_c			:1;     // 5   	 Software DC Link Voltage Under Fault
   Uint16 Vdc_Over			:1;     // 6     Software Vcap Fault
   Uint16 Speed			:1;     // 7     Software Speed Fault
   Uint16 Speed2			:1;     // 8     Software Speed Fault
   Uint16 X_rsvd			:7;     // 15:9  reserved
};

union SW_REG {
   Uint16              all;
   struct SW_BITS  bit;
};

typedef struct {
   union HW_REG  HW;        // Hardware Fault
   union SW_REG  SW;        // Sofrware Fault
} FAULT_REG;


extern FAULT_REG Fault;

void FaultProcess(void);
extern void InitPIE(void);
interrupt void fault(void);

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* #ifndef _FLT_ */
