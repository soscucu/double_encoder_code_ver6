#include <math.h>
#include "DSP28x_Project.h"
#include "filter.h"

#ifndef	DSP2834x_VARIABLE_H
#define	DSP2834x_VARIABLE_H

#define		NOP()	asm("	NOP	");	asm("	NOP	");	asm("	NOP	");	asm("	NOP	");	asm("	NOP	");asm("	NOP	")

#define SYS_CLK	300e6
#define SYS_CLK_PRD	3.33333333333333333e-9

//LED
#define Relay_ON			GpioDataRegs.GPBSET.bit.GPIO60 = 1
#define Relay_OFF			GpioDataRegs.GPBCLEAR.bit.GPIO60 = 1

#define Buffer_ON			GpioDataRegs.GPBCLEAR.bit.GPIO56 = 1
#define Buffer_OFF			GpioDataRegs.GPBSET.bit.GPIO56 = 1

#define LED2_ON				GpioDataRegs.GPBSET.bit.GPIO62 = 1
#define LED2_OFF			GpioDataRegs.GPBCLEAR.bit.GPIO62 = 1

#define LED3_ON				GpioDataRegs.GPBSET.bit.GPIO32 = 1
#define LED3_OFF			GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1

#define LED4_ON				GpioDataRegs.GPBSET.bit.GPIO49 = 1
#define LED4_OFF			GpioDataRegs.GPBCLEAR.bit.GPIO49 = 1

#define LED5_ON				GpioDataRegs.GPBSET.bit.GPIO52 = 1
#define LED5_OFF			GpioDataRegs.GPBCLEAR.bit.GPIO52 = 1

#define LED6_ON				GpioDataRegs.GPBSET.bit.GPIO59 = 1
#define LED6_OFF			GpioDataRegs.GPBCLEAR.bit.GPIO59 = 1

//ADC

#define ADC0_SOC_START		GpioDataRegs.GPBCLEAR.bit.GPIO46 = 1
#define ADC0_SOC_END		GpioDataRegs.GPBSET.bit.GPIO46 = 1

#define ADC1_SOC_START		GpioDataRegs.GPCCLEAR.bit.GPIO81 = 1
#define ADC1_SOC_END		GpioDataRegs.GPCSET.bit.GPIO81 = 1

#define ADC2_SOC_START		GpioDataRegs.GPCCLEAR.bit.GPIO80 = 1
#define ADC2_SOC_END		GpioDataRegs.GPCSET.bit.GPIO80 = 1

#define ADC0_BUSY			GpioDataRegs.GPBDAT.bit.GPIO47
#define ADC1_BUSY			GpioDataRegs.GPCDAT.bit.GPIO83
#define	ADC2_BUSY			GpioDataRegs.GPCDAT.bit.GPIO82

#define Vgrid_max		150.
#define INV_Vgrid_max	1./150.


//ENCODER
typedef struct
{
	Uint32 ppr;		// Pulse Per Revolution
	Uint32 mode;		// Multiplication Factor

	long pos;		// Encoder position

	Uint32 encTpos;
	long encPos;
	long encDelPos;
	Uint32 encDelTpos;
	float scale;	// Encoder Scale
	float speed;	// Encoder Speed

//	int noPulse;
//	int stat;		// Status of Input pulses
}ENCOBJ;

extern ENCOBJ encoder;

// Flag
typedef struct _Para_sw{
	int PC;
	int SC;
	int CC;
	int PCCalib;
	int SCCalib;
	int Controller_param_set;
	int Relay;
	int Buffer;
	int Double_sampling;
	int Align;
} Switch;

extern Switch SW1;
// Flag
typedef struct _Para_flag{

	int Relay;
	int Buffer; //turn on manualy, turn off automatically
	int LED2;
	int LED3;
	int LED4;
	int LED5;
	int LED6;
	int Manual_switch;

} watch_point;

extern watch_point Flag;

typedef struct _Para_Motor_
{
	/////////////Motor Paramter////////////
	int 	Pole, PolePair;
	float 	InvPolePair;
	float 	Rs, Ls, INV_Ls, Ke, Lds, Lqs, LAMpm, INV_LAMpm, INV_Ls_Tsw, Ls_Fs;
	float 	Te_rated, Is_rated;
	float	Wrm_rated, Wrm_slope_rate;
	float 	Pout_rated;
	float 	Jm, Bm, InvJm;
	float 	Kt,INV_Kt;


	/////////////State Variable////////////

	float 	Thetar, Thetar_SQ, Thetam, ThetaAdv, ThetaOffset;
	float 	Cos_ThetaAdv, Sin_ThetaAdv;
	float 	Wr, Wrm, Wrpm;
	float 	Idss, Iqss, Idse, Iqse;
	float 	Vdc, Ia, Ib, Ic;
	float   SinTheta, CosTheta, Theta_sq;
	float   SinTheta_predict, CosTheta_predict, Thetar_predict,Thetar_SQ_predict;

	///////////Control variable////////////
	float   Wrm_Err, Wrm_ref, Wrm_ref_set, Wrm_ref_set_prev;
	float 	Thetam_ref, Thetam_Err, Thetam_Err_True;
	float	Thetam_ref_set, Thetam_ref_set_prev, InitThetar, Thetam_temp;
	float	Thetam_Err_diff,	Thetam_Err_prev;

	float 	Te_ref, Te_integ, Te_anti;
	float	acc_ref_ff, Wrm_ref_ff;
	float	Idse_ref_set, Iqse_ref_set ;
	float	Idse_ref, Iqse_ref;
	float	Err_Idse, Err_Iqse;
	float 	Idse_integ,Iqse_integ;

	///////////Controller gain////////////
	float	Wsc, Fsc;
	float 	Wpc, Fpc;
	float	Kpd, Kpq, Kad, Kaq, Kid_T, Kiq_T;
	float	Ka_sc, Kp_sc, Ki_scT, Ki_sc;
	float 	zeta_sc;
	float 	Kp_pc, Kd_pc, Kd_pcT;

	float  	Vdse, Vqse;
	float   Vdse_ref, Vqse_ref;
	float   Vdse_ref_fb, Vqse_ref_fb;
	float   Vdse_ref_ff, Vqse_ref_ff;
	float   Vdss_ref, Vqss_ref, Vdss_ref_old, Vqss_ref_old;
	float   Vdse_ref_set, Vqse_ref_set;
	float   Vdss_ref_set, Vqss_ref_set;
	float   Vas_ref, Vbs_ref, Vcs_ref;
	float   Vas, Vbs, Vcs;
	float   Vdss, Vqss;
	float   Van_ref_set, Vbn_ref_set, Vcn_ref_set;
	float   Vdqse_ref;
	float	Vdse_ref_set_ave,Vqse_ref_set_ave;
	float	Vdse_ref_lim,Vqse_ref_lim;
	float	Vdse_ref_set_old,Vqse_ref_set_old;
	float	Vdss_ref_set_old,Vqss_ref_set_old;
	float	Vas_ref_set_old,Vbs_ref_set_old,Vcs_ref_set_old;

	float Van_diff, Vbn_diff, Vcn_diff;

	float Vdss_anti, Vqss_anti;
	float 	Vdse_anti, Vqse_anti;

	int   CompA_add,CompB_add,CompC_add;
	int   CompA_ZCC,CompB_ZCC,CompC_ZCC;
	int   Comp_DEAD;
	int	  Comp_posA_DEAD;
	int	  Comp_negA_DEAD;
	int	  Comp_posB_DEAD;
	int	  Comp_negB_DEAD;
	int	  Comp_posC_DEAD;
	int	  Comp_negC_DEAD;

	float INV_Icomp;
	float INV_pos_Icomp;
	float INV_neg_Icomp;

	float	Van_ref, Vbn_ref, Vcn_ref;

	float	alpha, Wcc, Fcc;
	float	Idqse_ref, Te_real;
	float   Ke_filter_d, Ke_filter_q;
	float   Ke_d,Ke_q;
	float   Ke_d_sum,Ke_q_sum;
	float   Ke_d_sum_filter,Ke_q_sum_filter;

	float   Ke_est;


	float 	Rv;
	float	Vmax, Vmin, Vsn;
	float   Thetar_num;

	unsigned int		CompA, CompB, CompC;

	float Iqse_ref_max;
	float Idse_ref_max;

	float Err_flux, flux_integ, Kif_T, Kaf, Kpf, flux_anti, flux_ref, flux_ref_lim, flux_ref_min;
	float Thetar_filtered;

	float  Thetam_prev, addedThetar;

} Motor;
extern Motor Inv1;

//delaycc function
extern float Tdelay;
extern unsigned short cnt_delay;
extern int offsetLoopCnt, offsetMaxCnt;

// Clock
extern float PWM_CLK;
extern float Fsw, Fsamp;
extern float Tsamp;

extern 	int IERBackupCC;
//cc.c test variable
extern float test_pwm_duty4;
extern float test_pwm_duty5;
extern float test_pwm_duty6;


//Timer variable
extern long ccTimer2start;
extern float Time_CC;

//PWM variable
extern unsigned int uTsPerTdsp;
extern Uint16 maxCount_ePWM;

//encoder
extern int IERBackup_EQ;
extern int eqep_offset_done;
extern long Theta_offset2;
extern float eqep_offset_toogle;
extern Uint32 EncoderCount2;


extern void delaycc(float time);
extern void initEnc ( ENCOBJ *p ,  int ppr, int mode);

extern void InitParameter(void);
extern void InitController(void);
extern void InitSW(void);
extern void InitFlag(void);

// PWM Buffer Control
extern void Switch_DSP_On(void);
extern void Switch_DSP_Off(void);
extern void SwitchOn(void);
extern void SwitchOff(void);


extern void InitUIGpio(void);




#endif
