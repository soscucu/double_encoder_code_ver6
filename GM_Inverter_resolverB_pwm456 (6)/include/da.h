#ifndef	DSP2834x_DA
#define	DSP2834x_DA

extern long *da[4];
extern int  da_type[4], da_data[4];
extern float da_scale[4], da_mid[4], da_temp[4];
extern int DacOffset;

void InitSerialDac(void);
void SerialDacOut(void);

#endif
