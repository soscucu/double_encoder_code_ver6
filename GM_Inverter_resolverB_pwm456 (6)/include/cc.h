#ifndef	DSP2834x_CC_H_
#define	DSP2834x_CC_H_
	
#include "variable.h"
#include "comp.h"

extern interrupt void cc(void);
extern void Frame_Transform(float* Xs, float* Ys, float* Xe, float* Ye, const float Cos, const float Sin, Uint16 s2eOre2s);
extern void PLL_Process(void);
extern void Transformation(void);
extern void VC_Process(void);
extern void CC_Process(void);
extern void PwmUpdate(void);

#endif
