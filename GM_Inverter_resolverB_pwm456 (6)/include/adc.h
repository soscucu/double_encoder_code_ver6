#ifndef DSP2834x_ADC_H
#define DSP2834x_ADC_H

extern void InitAdc(void);
extern void ADC_Process(void);
extern void ADCOffset(void);

extern Uint16 ADCsetVal;
extern int ADCH[10];
extern float ScaleAin[10];
extern float OffsetAin[10];

#endif  // end of DSP2833x_ADC_H definition


//===========================================================================
// End of file.
//===========================================================================
