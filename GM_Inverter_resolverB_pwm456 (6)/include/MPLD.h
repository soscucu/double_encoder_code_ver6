/****************************************************
MPLD.h
*****************************************************/

#ifndef DSP2834x_MPLD_H__
#define DSP2834x_MPLD_H__

#define XINTF_ZONE6		0x100000
#define XINTF_ZONE7		0x200000

//#define MPLD_ADDR		XINTF_ZONE7
#define MPLD_ADDR	XINTF_ZONE7

#define AD_BASE			(MPLD_ADDR + 0x1000)
	#define	AD_CS_BASE		(AD_BASE + 0x0000)
		#define	AD_RD0			(*(volatile int *)(AD_CS_BASE + 0x0000))
		#define	AD_RD1			(*(volatile int *)(AD_CS_BASE + 0x0002))
		#define	AD_RD2			(*(volatile int *)(AD_CS_BASE + 0x0004))
		#define	AD_RD3			(*(volatile int *)(AD_CS_BASE + 0x0006))
		#define	AD_RD4			(*(volatile int *)(AD_CS_BASE + 0x0008))
		#define	AD_RD5			(*(volatile int *)(AD_CS_BASE + 0x000A))
		#define	AD_RD6			(*(volatile int *)(AD_CS_BASE + 0x000C))
		#define	AD_RD7			(*(volatile int *)(AD_CS_BASE + 0x000E))
		#define	AD_RD8			(*(volatile int *)(AD_CS_BASE + 0x0010))
	#define	AD_SOC_BASE		(AD_BASE + 0x0100)
		#define	AD_RST	    (*(volatile long *)(AD_SOC_BASE + 0x0000))
		#define AD_CV	    	(*(volatile int *)(AD_SOC_BASE + 0x0002))
		#define AD_CVOFF	(*(volatile int *)(AD_SOC_BASE + 0x0004))
	#define AD_RD_BASE	    (AD_BASE + 0x0200)
		#define AD_BS 		(*(volatile long *)(AD_RD_BASE + 0x0000))



////////////////////PWM ADDRESS///////////////////////////////////
#define PWM_BASE			(MPLD_ADDR + 0x2000)
	#define PWM0_BASE	(PWM_BASE+0x0000)
		#define	PWM0_CTRL		(*(volatile int *)(PWM0_BASE + 0x000))
		#define	PWM0_PRD		(*(volatile int *)(PWM0_BASE + 0x002))
		#define	PWM0_CNT		(*(volatile int *)(PWM0_BASE + 0x004))
		#define	PWM0_PHASE		(*(volatile int *)(PWM0_BASE + 0x006))
		#define	PWM0_CMPA		(*(volatile int *)(PWM0_BASE + 0x008))//-- we can use even number address  
		#define	PWM0_CMPB		(*(volatile int *)(PWM0_BASE + 0x00A))
		#define	PWM0_CMPC		(*(volatile int *)(PWM0_BASE + 0x00C))	
		#define	PWM0_DTIME		(*(volatile int *)(PWM0_BASE + 0x00E))
		#define	PWM0_STAT		(*(volatile int *)(PWM0_BASE + 0x010))
//		#define	PWM0_DELAY		(*(volatile int *)(PWM0_BASE + 0x012))
	#define PWM1_BASE	(PWM_BASE+0x0100)
		#define	PWM1_CTRL		(*(volatile int *)(PWM1_BASE + 0x000))
		#define	PWM1_PRD		(*(volatile int *)(PWM1_BASE + 0x002))
		#define	PWM1_CNT		(*(volatile int *)(PWM1_BASE + 0x004))
		#define	PWM1_PHASE		(*(volatile int *)(PWM1_BASE + 0x006))
		#define	PWM1_CMPA		(*(volatile int *)(PWM1_BASE + 0x008))
		#define	PWM1_CMPB		(*(volatile int *)(PWM1_BASE + 0x00A))
		#define	PWM1_CMPC 		(*(volatile int *)(PWM1_BASE + 0x00C))
		#define	PWM1_DTIME		(*(volatile int *)(PWM1_BASE + 0x00E))
		#define	PWM1_STAT		(*(volatile int *)(PWM1_BASE + 0x010))
//		#define	PWM1_DELAY		(*(volatile int *)(PWM1_BASE + 0x012))
	#define PWM2_BASE	(PWM_BASE+0x0200)
		#define	PWM2_CTRL		(*(volatile int *)(PWM2_BASE + 0x000))
		#define	PWM2_PRD		(*(volatile int *)(PWM2_BASE + 0x002))
		#define	PWM2_CNT		(*(volatile int *)(PWM2_BASE + 0x004))
		#define	PWM2_PHASE		(*(volatile int *)(PWM2_BASE + 0x006))
		#define	PWM2_CMPA		(*(volatile int *)(PWM2_BASE + 0x008))
		#define	PWM2_CMPB		(*(volatile int *)(PWM2_BASE + 0x00A))
		#define	PWM2_CMPC 		(*(volatile int *)(PWM2_BASE + 0x00C))
		#define	PWM2_DTIME		(*(volatile int *)(PWM2_BASE + 0x00E))


	#define FAULT_BASE			(MPLD_ADDR + 0x3000)
		#define	nTotalFLTSel		(*(volatile int *)(FAULT_BASE + 0x0000))
		#define nCurrFLTSel  		(*(volatile int *)(FAULT_BASE + 0x0002))
		#define nVoltFLTSel			(*(volatile int *)(FAULT_BASE + 0x0004))
		#define nGateFLTSel			(*(volatile int *)(FAULT_BASE + 0x0006))
		#define	FAULT_CLR	    	(*(volatile int *)(FAULT_BASE + 0x0008))
		#define	FAULT_MASK	    	(*(volatile int *)(FAULT_BASE + 0x000A))
	
		#define nTotal_buf			(*(volatile int *)(FAULT_BASE + 0x0020))
	
	
////////////////////DA ADDRESS///////////////////////////////////
#define DA_BASE			(MPLD_ADDR + 0x4000)
	#define DA0_CS_BASE	(DA_BASE+0x0000)
		#define nDA0_CS0		(*(volatile int *)(DA0_CS_BASE+0x0000))
		#define nDA0_CS1		(*(volatile int *)(DA0_CS_BASE+0x0010))
		#define nDA0_CS2		(*(volatile int *)(DA0_CS_BASE+0x0020))
		#define nDA0_CS3		(*(volatile int *)(DA0_CS_BASE+0x0030))
		
	#define DA0_SOC_BASE	(DA_BASE+0x0100)
		#define DA0_SOC 		(*(volatile int *)(DA0_SOC_BASE + 0x0000))
		#define DA0_SOC_OFF		(*(volatile int *)(DA0_SOC_BASE + 0x0002))

	#define DA0_mode (DA_BASE+0x0400)
		#define nDA0_mode0 	(*(volatile int *)(DA0_mode + 0x0000))
		#define nDA0_mode1 (*(volatile int *)(DA0_mode + 0x0002))

	#define Resol_BASE			(MPLD_ADDR + 0x6000)
		#define Resol_CS_BASE	(Resol_BASE+0x0000)
			#define	Resolver_RD			(*(volatile int *)(Resol_CS_BASE + 0x0002))

#endif	// of _MPLD_H__



