/***************************************************************
	COMP.H
	
	programmed by Yo-chan Son, Oct 2000.
	copyright (c) 1991, 2000 by EEPEL, SNU, SEOUL, KOREA
	All Rights Reserved.
****************************************************************/

#ifndef	DSP2834x__COMP
#define	DSP2834x__COMP

//Triangular Function constant
#define f2      	((float)(1./2.))
#define	f3			((float)(f2/3.))
#define	f4			((float)(f3/4.))
#define	f5			((float)(f4/5.))
#define	f6			((float)(f5/6.))
#define	f7			((float)(f6/7.))
#define	f8			((float)(f7/8.))
#define	f9			((float)(f8/9.))
#define	f10			((float)(f9/10.))
#define	f11			((float)(f10/11.))
#define	f12			((float)(f11/12.))
#define	f13			((float)(f12/13.))
#define	f14			((float)(f13/14.))
#define	f15			((float)(f14/15.))

// Floating-point Constants
#define	PI			((float)3.141592653589793)
#define	PI2			((float)6.283185307179586)
#define	PI_THIRD	((float)1.047197551196598)
#define INV_2PI		((float)0.159154943091895)
#define INV_PI		((float)0.318309886183791)
#define	SQRT2		((float)1.414213562373095)
#define	SQRT3		((float)1.732050807568877)
#define	SQRT3HALF	((float)0.866025403784438)
#define	INV_SQRT2	((float)0.707106781186547)
#define	INV_SQRT3	((float)0.577350269189626)
#define	INV_3		((float)0.333333333333333)
#define Rm2Rpm		((float)9.549296585513720)
#define Rpm2Rm		((float)0.104719755119659)

// Macro Functions
#define LIMIT(x,s,l)			(((x)>(l))?(l):((x)<(s))?((s)):(x))
#define	MAX(a, b)				((a)>(b) ? (a) : (b))
#define	MIN(a, b)				((a)>(b) ? (b) : (a))
#define	BOUND_PI(x)             ((x>0)?((x)-2.*PI*(int)((x+PI)/(2.*PI))):((x)-2.*PI*(int)((x-PI)/(2.*PI))))
#define	ABS(x)					((x>0)?(x):(-x))
#define	SIGN(x)					((x<0)? -1. : 1. )
#define SIN(x,x2)		((x)*((float)1.-(x2)*(f3-(x2)*(f5-(x2)*(f7-(x2)*(f9-(x2)*(f11-(x2)*(f13-(x2)*f15))))))))
#define COS(x2)			((float)1.-(x2)*(f2-(x2)*(f4-(x2)*(f6-(x2)*(f8-(x2)*(f10-(x2)*(f12-(x2)*f14)))))))
#define SIN_INV_X(x2)   (((float)1.-(x2)*(f3-(x2)*(f5-(x2)*(f7-(x2)*(f9-(x2)*(f11-(x2)*(f13-(x2)*f15))))))))
#define EXP(x)			((float)1.+(x)*((float)1.+(x)*(f2+(x)*(f3+(x)*(f4+(x)*(f5+(x)*(f6+(x)*f7)))))))

#endif
