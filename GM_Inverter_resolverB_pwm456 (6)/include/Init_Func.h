/**********************************************************************
* File: SNU_28377D__Init_Func_Prototypes.h
* Device: TMS320F2837xD
* Author: Technical Training Organization (TTO), Texas Instruments
* Description: Include file for C28x workshop labs.  Include this
* file in all C-source files.
**********************************************************************/

#ifndef F2834x_INIT_FUNC_H
#define F2834x_INIT_FUNC_H


#endif  // end of F2837xS_INIT_FUNC_H definition
