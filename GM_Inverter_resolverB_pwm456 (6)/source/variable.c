#include "MPLD.h"
#include "variable.h"
#include "comp.h"

#define ON 1
#define OFF 0



//delaycc function
float Tdelay=1.e-6;
unsigned short cnt_delay;
int offsetLoopCnt = 0, offsetMaxCnt = 10000;//10000;	// power of 2, maximum = 2^18

// Clock
float PWM_CLK = 300.e6;
float Fsw = 10.e3, Fsamp = 10.e3;
float Tsamp = 0.;


int IERBackupCC;
///////////////cc.c///////////////////////
// test variable
float test_pwm_duty4 = 0.;
float test_pwm_duty5 = 0.;
float test_pwm_duty6 = 0.;

//Timer variable
long ccTimer2start = 0;
float Time_CC = 0.;

//PWM variable
Uint16 maxCount_ePWM = 0;


//encoder
ENCOBJ encoder;
int IERBackup_EQ = 0;
int eqep_offset_done = 0;
long Theta_offset2 = 0.;
float eqep_offset_toogle = 0.;
Uint32 EncoderCount2 = 0;

//Flag


Switch SW1;
watch_point Flag;
Motor Inv1;
void delaycc(float time)
{
	cnt_delay=(unsigned short)(time*SYS_CLK);
	asm(" RPT @_cnt_delay	|| NOP ");
	asm(" NOP		");
}

void initEnc ( ENCOBJ *p,  int ppr, int mode){
	p->encDelPos = 0;
	p->encDelTpos = 0;
	p->encPos = 0;
	p->encTpos = 0;
	p->mode = mode;
	p->pos = 0;
	p->ppr = ppr;
	p->scale = 2*PI/(p->ppr * p->mode);
	p->speed = 0;
}

void InitParameter(void){

}

void InitController(void){

}


void InitSW(void){
	SW1.PC = OFF;
	SW1.SC = OFF;
	SW1.CC = OFF;
	SW1.PCCalib = OFF;
	SW1.SCCalib = OFF;
	SW1.Controller_param_set = OFF;
	SW1.Relay = OFF;
	SW1.Buffer = OFF;
	SW1.Double_sampling = OFF;
	SW1.Align = OFF;
}


void InitFlag(void){
	Flag.Relay = OFF;
	Flag.Buffer = OFF; //turn on manualy, turn off automatically
	Flag.LED2 = OFF;
	Flag.LED3 = OFF;
	Flag.LED4 = OFF;
	Flag.LED5 = OFF;
	Flag.LED6 = OFF;
	Flag.Manual_switch = OFF;
}
// PWM Buffer Control
void Switch_DSP_On(void){

	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;

}

void Switch_DSP_Off(void){

	EPwm1Regs.AQSFRC.bit.OTSFA = 1; //sw force event single trigger on
	EPwm1Regs.AQSFRC.bit.OTSFB = 1; //sw force event single trigger on
	EPwm2Regs.AQSFRC.bit.OTSFA = 1; //sw force event single trigger on
	EPwm2Regs.AQSFRC.bit.OTSFB = 1; //sw force event single trigger on
	EPwm3Regs.AQSFRC.bit.OTSFA = 1; //sw force event single trigger on
	EPwm3Regs.AQSFRC.bit.OTSFB = 1; //sw force event single trigger on

	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;

	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;

	EPwm1Regs.CMPA.half.CMPA = 0;
	EPwm2Regs.CMPA.half.CMPA = 0;
	EPwm3Regs.CMPA.half.CMPA = 0;
}
void SwitchOn(void)
{
	nTotal_buf = 0x03;  //0000 0011 Active high
}

void SwitchOff(void)
{
	nTotal_buf = 0x00;  //0000 0000 Active high
}


void InitUIGpio(void){
	EALLOW;

	GpioCtrlRegs.GPAMUX1.bit.GPIO11  = 0;	//GPIO11 - Flag_cc switch
	GpioCtrlRegs.GPADIR.bit.GPIO11   = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO11 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO11   = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO32  = 0;	//GPIO32 - LED3
	GpioCtrlRegs.GPBDIR.bit.GPIO32   = 1;	//Output
	GpioCtrlRegs.GPBQSEL1.bit.GPIO32 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO32   = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO49  = 0;	//GPIO49 - LED4
	GpioCtrlRegs.GPBDIR.bit.GPIO49   = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO49 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO49   = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO52  = 0;	//GPIO52 - LED5
	GpioCtrlRegs.GPBDIR.bit.GPIO52   = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO52 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO52   = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO56  = 0;	//GPIO56 - buffer
	GpioDataRegs.GPBSET.bit.GPIO56   = 1;   //set to buffer disable
	GpioCtrlRegs.GPBDIR.bit.GPIO56   = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO56 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO56   = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO59  = 0;	//GPIO59 - LED6
	GpioCtrlRegs.GPBDIR.bit.GPIO59   = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO59 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO59   = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO60  = 0;	//GPIO60 - Relay
	GpioDataRegs.GPBCLEAR.bit.GPIO60 = 1;   //set to buffer disable
	GpioCtrlRegs.GPBDIR.bit.GPIO60   = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO60 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO60   = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO62  = 0;	//GPIO62 - LED2
	GpioCtrlRegs.GPBDIR.bit.GPIO62   = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO62 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO62   = 0;
    EDIS;
}

///////////////cc.c/////////////////////////
float EncoderAngleGenerator(int EQep_num){
	float temp_thetar_enc = 0.;
	int EncoderCountX = 0;
	switch(EQep_num){
	case 1 :
		EncoderCountX =  EQep1Regs.QPOSCNT;
		temp_thetar_encX = (float)EncoderCountX * (float)encoder1.scale;
		break;
	case 2 :
		EncoderCountX =  EQep2Regs.QPOSCNT;
		temp_thetar_encX = (float)EncoderCountX * (float)encoder2.scale;
		break;
	}

	return temp_thetar_encX;

}



/*----------------------------------------------------------------------*/
/*         End of VARIABLE.C											*/
/*----------------------------------------------------------------------*/

