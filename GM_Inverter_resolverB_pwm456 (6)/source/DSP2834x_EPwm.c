#include "DSP28x_Project.h"
#include "variable.h"


void ConfigureEPWM(volatile struct EPWM_REGS *EPwmXRegs, Uint16 maxCount_ePWM, Uint16 Initial_CMP, float Deadband, Uint16 Counter_mode, Uint16 Sampling_mode);

void InitEPwm(void)
{

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;      // Stop all the TB clocks
	EDIS;

	maxCount_ePWM =	(Uint16)(PWM_CLK/Fsamp)>>1; // Master carrier
	maxCount_ePWM = ((maxCount_ePWM>>1)<<1);

	InitEPwmGpio();

	ConfigureEPWM(&EPwm1Regs, maxCount_ePWM, 0, 3.e-6, TB_COUNT_UPDOWN, ET_CTR_ZERO); // Master
	ConfigureEPWM(&EPwm2Regs, maxCount_ePWM, 0, 3.e-6, TB_COUNT_UPDOWN, ET_CTR_PRD); // Slave
	ConfigureEPWM(&EPwm3Regs, maxCount_ePWM, 0, 3.e-6, TB_COUNT_UPDOWN, ET_CTR_ZERO); // Slave
	ConfigureEPWM(&EPwm4Regs, maxCount_ePWM, 0, 3.e-6, TB_COUNT_UPDOWN, ET_CTR_ZERO); // Slave
	ConfigureEPWM(&EPwm5Regs, maxCount_ePWM, 0, 3.e-6, TB_COUNT_UPDOWN, ET_CTR_ZERO); // Slave
	ConfigureEPWM(&EPwm6Regs, maxCount_ePWM, 0, 3.e-6, TB_COUNT_UPDOWN, ET_CTR_ZERO); // Slave

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;         // Start all the timers synced
	EDIS;
}

void InitEPwmGpio(void)
{
   InitEPwm1Gpio();
   InitEPwm2Gpio();
   InitEPwm3Gpio();
   InitEPwm4Gpio();
   InitEPwm5Gpio();
   InitEPwm6Gpio();
   //InitTzGpio();
}

void ConfigureEPWM(volatile struct EPWM_REGS *EPwmXRegs, Uint16 maxCount_ePWM, Uint16 Initial_CMP, float Deadband, Uint16 Counter_mode, Uint16 Sampling_mode)
{
	EALLOW;
	//TZ setting
	EPwmXRegs->TZCLR.bit.OST = 1;
	EPwmXRegs->TZCLR.bit.CBC = 1;
	EPwmXRegs->TZCLR.bit.INT = 1;


	EPwmXRegs->TZSEL.bit.CBC4 = 1; // nTZ4 is trip source for EPwmX
	EPwmXRegs->TZCTL.bit.TZA = TZ_FORCE_LO;
	EPwmXRegs->TZCTL.bit.TZB = TZ_FORCE_LO;
	EPwmXRegs->TZEINT.bit.CBC = 1;

	// Setup Counter
	EPwmXRegs->TBPRD = maxCount_ePWM;			// Set Period
	EPwmXRegs->CMPA.half.CMPA = Initial_CMP;
	EPwmXRegs->TBPHS.all = 0;
	EPwmXRegs->TBCTR = 0x0000;

	// Setup counter mode
	EPwmXRegs->TBCTL.bit.CTRMODE = Counter_mode;
	EPwmXRegs->TBCTL.bit.PHSEN = TB_DISABLE;
	EPwmXRegs->TBCTL.bit.PRDLD = TB_SHADOW;

	// Sync
	EPwmXRegs->TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;

	// Setup counter clock
	EPwmXRegs->TBCTL.bit.HSPCLKDIV =TB_DIV1;  	//TBCLK = EPWMCLK/(HSPCLKDIV * CLKDIV)
	EPwmXRegs->TBCTL.bit.CLKDIV = TB_DIV1;

	//Setup shadowing
	EPwmXRegs->CMPCTL.bit.SHDWAMODE = CC_SHADOW; // Set shadow mode
	EPwmXRegs->CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwmXRegs->CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
	EPwmXRegs->CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

	//Set manual fault actions
	EPwmXRegs->AQCTLA.bit.CAU = AQ_CLEAR;
	EPwmXRegs->AQSFRC.bit.OTSFA = 1; //sw force event single trigger on
	EPwmXRegs->AQSFRC.bit.OTSFB = 1; //sw force event single trigger on

	EPwmXRegs->AQSFRC.bit.RLDCSF = 3; //
	EPwmXRegs->AQSFRC.bit.ACTSFA = 1; //Clear (LOW)
	EPwmXRegs->AQSFRC.bit.ACTSFB = 1; //Clear (LOW)

	//Dead Band
	EPwmXRegs->DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwmXRegs->DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwmXRegs->DBCTL.bit.IN_MODE = DBA_ALL;
	EPwmXRegs->DBRED = (Uint16)(Deadband*PWM_CLK);
	EPwmXRegs->DBFED = (Uint16)(Deadband*PWM_CLK);

	//Interrupt
	EPwmXRegs->ETSEL.bit.INTEN = 1;              	// Enable INT
	EPwmXRegs->ETSEL.bit.INTSEL = Sampling_mode;
	EPwmXRegs->ETPS.bit.INTPRD = ET_1ST;    		// Generate INT on 1st event

	EDIS;
}

void InitEPwm1Gpio(void)
{
   EALLOW;
   GpioCtrlRegs.GPAPUD.bit.GPIO0 = 1;    // Disable pull-up on GPIO0 (EPWM1A)
   GpioCtrlRegs.GPAPUD.bit.GPIO1 = 1;    // Disable pull-up on GPIO1 (EPWM1B)
   GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1;   // Configure GPIO0 as EPWM1A
   GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 1;   // Configure GPIO1 as EPWM1B
   EDIS;
}

void InitEPwm2Gpio(void)
{
   EALLOW;
   GpioCtrlRegs.GPAPUD.bit.GPIO2 = 1;    // Disable pull-up on GPIO2 (EPWM2A)
   GpioCtrlRegs.GPAPUD.bit.GPIO3 = 1;    // Disable pull-up on GPIO3 (EPWM2B)
   GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 1;   // Configure GPIO2 as EPWM2A
   GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 1;   // Configure GPIO3 as EPWM2B
   EDIS;
}

void InitEPwm3Gpio(void)
{
   EALLOW;
   GpioCtrlRegs.GPAPUD.bit.GPIO4 = 1;    // Disable pull-up on GPIO4 (EPWM3A)
   GpioCtrlRegs.GPAPUD.bit.GPIO5 = 1;    // Disable pull-up on GPIO5 (EPWM3B)
   GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 1;   // Configure GPIO4 as EPWM3A
   GpioCtrlRegs.GPAMUX1.bit.GPIO5 = 1;   // Configure GPIO5 as EPWM3B
   EDIS;
}

void InitEPwm4Gpio(void)
{
   EALLOW;
   GpioCtrlRegs.GPAPUD.bit.GPIO6 = 1;    // Disable pull-up on GPIO6 (EPWM4A)
   GpioCtrlRegs.GPAPUD.bit.GPIO7 = 1;    // Disable pull-up on GPIO7 (EPWM4B)
   GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 1;   // Configure GPIO6 as EPWM4A
   GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 1;   // Configure GPIO7 as EPWM4B
   EDIS;
}

void InitEPwm5Gpio(void)
{
   EALLOW;
   GpioCtrlRegs.GPAPUD.bit.GPIO8 = 1;    // Disable pull-up on GPIO8 (EPWM5A)
   GpioCtrlRegs.GPAPUD.bit.GPIO9 = 1;    // Disable pull-up on GPIO9 (EPWM5B)
   GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 1;   // Configure GPIO8 as EPWM5A
   GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 1;   // Configure GPIO9 as EPWM5B
   EDIS;
}

void InitEPwm6Gpio(void)
{
   EALLOW;
   GpioCtrlRegs.GPAPUD.bit.GPIO10 = 1;    // Disable pull-up on GPIO10 (EPWM6A)
   GpioCtrlRegs.GPAPUD.bit.GPIO11 = 1;    // Disable pull-up on GPIO11 (EPWM6B)
   GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 1;   // Configure GPIO10 as EPWM6A
   GpioCtrlRegs.GPAMUX1.bit.GPIO11 = 1;   // Configure GPIO11 as EPWM6B
   EDIS;
}

void InitTzGpio(void)
{
   EALLOW;
   GpioCtrlRegs.GPAPUD.bit.GPIO12 = 0;    // Enable pull-up on GPIO12 (TZ1)
   GpioCtrlRegs.GPAPUD.bit.GPIO13 = 0;    // Enable pull-up on GPIO13 (TZ2)
   GpioCtrlRegs.GPAPUD.bit.GPIO14 = 0;    // Enable pull-up on GPIO14 (TZ3)
   GpioCtrlRegs.GPAPUD.bit.GPIO15 = 0;    // Enable pull-up on GPIO15 (TZ4)

   GpioCtrlRegs.GPAQSEL1.bit.GPIO12 = 3;  // Asynch input GPIO12 (TZ1)
   GpioCtrlRegs.GPAQSEL1.bit.GPIO13 = 3;  // Asynch input GPIO13 (TZ2)
   GpioCtrlRegs.GPAQSEL1.bit.GPIO14 = 3;  // Asynch input GPIO14 (TZ3)
   GpioCtrlRegs.GPAQSEL1.bit.GPIO15 = 3;  // Asynch input GPIO15 (TZ4)

   GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 1;  // Configure GPIO12 as TZ1
   GpioCtrlRegs.GPAMUX1.bit.GPIO13 = 1;  // Configure GPIO13 as TZ2
   GpioCtrlRegs.GPAMUX1.bit.GPIO14 = 1;  // Configure GPIO14 as TZ3
   GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 1;  // Configure GPIO14 as TZ3
   EDIS;
}
//===========================================================================
// End of file.
//===========================================================================
