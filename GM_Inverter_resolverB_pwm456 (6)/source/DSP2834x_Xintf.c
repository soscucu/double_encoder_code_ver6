// TI File $Revision: /main/5 $
// Checkin $Date: August 28, 2008   16:54:21 $
//###########################################################################
//
// FILE:   DSP2834x_Xintf.c
//
// TITLE:   DSP2834x Device External Interface Init & Support Functions.
//
// DESCRIPTION:
//
//          Example initialization function for the external interface (XINTF).
//          This example configures the XINTF to its default state.  For an
//          example of how this function being used refer to the
//          examples/run_from_xintf project.
//
//###########################################################################
// $TI Release: DSP2834x C/C++ Header Files V1.10 $
// $Release Date: July 27, 2009 $
//###########################################################################

#include "DSP2834x_Device.h"     // DSP2834x Headerfile Include File
#include "DSP2834x_Examples.h"   // DSP2834x Examples Include File

//---------------------------------------------------------------------------
// InitXINTF:
//---------------------------------------------------------------------------
// This function initializes the External Interface the default reset state.
//
// Do not modify the timings of the XINTF while running from the XINTF.  Doing
// so can yield unpredictable results

void XintfInit(void)
{
	InitXintf16Gpio();

    EALLOW;
    XintfRegs.XINTCNF2.bit.WRBUFF = 3;

	XintfRegs.XTIMING7.bit.XWRLEAD = 1;
    XintfRegs.XTIMING7.bit.XWRACTIVE = 2;//3;
    XintfRegs.XTIMING7.bit.XWRTRAIL = 1;//3;

    XintfRegs.XTIMING7.bit.XRDLEAD = 1;
    XintfRegs.XTIMING7.bit.XRDACTIVE = 3;
    XintfRegs.XTIMING7.bit.XRDTRAIL = 1;//3;

    XintfRegs.XTIMING7.bit.X2TIMING = 0;//1;

    XintfRegs.XTIMING7.bit.USEREADY = 1;
    XintfRegs.XTIMING7.bit.READYMODE = 0;  // sample asynchronous

    XintfRegs.XTIMING7.bit.XSIZE = 1;

    XintfRegs.XBANK.bit.BANK = 7;
    XintfRegs.XBANK.bit.BCYC = 0;
    EDIS;

   asm(" RPT #7 || NOP");

}

void InitXintf16Gpio()
{
     EALLOW;
     GpioCtrlRegs.GPCMUX1.bit.GPIO64 = 2;  // XD15
     GpioCtrlRegs.GPCMUX1.bit.GPIO65 = 2;  // XD14
     GpioCtrlRegs.GPCMUX1.bit.GPIO66 = 2;  // XD13
     GpioCtrlRegs.GPCMUX1.bit.GPIO67 = 2;  // XD12
     GpioCtrlRegs.GPCMUX1.bit.GPIO68 = 2;  // XD11
     GpioCtrlRegs.GPCMUX1.bit.GPIO69 = 2;  // XD10
     GpioCtrlRegs.GPCMUX1.bit.GPIO70 = 2;  // XD9
     GpioCtrlRegs.GPCMUX1.bit.GPIO71 = 2;  // XD8
     GpioCtrlRegs.GPCMUX1.bit.GPIO72 = 2;  // XD7
     GpioCtrlRegs.GPCMUX1.bit.GPIO73 = 2;  // XD6
     GpioCtrlRegs.GPCMUX1.bit.GPIO74 = 2;  // XD5
     GpioCtrlRegs.GPCMUX1.bit.GPIO75 = 2;  // XD4
     GpioCtrlRegs.GPCMUX1.bit.GPIO76 = 2;  // XD3
     GpioCtrlRegs.GPCMUX1.bit.GPIO77 = 2;  // XD2
     GpioCtrlRegs.GPCMUX1.bit.GPIO78 = 2;  // XD1
     GpioCtrlRegs.GPCMUX1.bit.GPIO79 = 2;  // XD0

     GpioCtrlRegs.GPBMUX1.bit.GPIO40 = 2;  // XA0 (Note: XWE1n is a seperate pin and not muxed to XA0 in C2834x/C2824x)
     GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 2;  // XA1
     GpioCtrlRegs.GPBMUX1.bit.GPIO42 = 2;  // XA2
     GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 2;  // XA3
     GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 2;  // XA4
     GpioCtrlRegs.GPBMUX1.bit.GPIO45 = 2;  // XA5
     GpioCtrlRegs.GPBMUX1.bit.GPIO46 = 0;  // ADCSOC0 //2;  // XA6
     GpioCtrlRegs.GPBMUX1.bit.GPIO47 = 0;  // ADCBZ0 //2;  // XA7
     GpioCtrlRegs.GPCMUX2.bit.GPIO80 = 0;  //2;  // XA8
     GpioCtrlRegs.GPCMUX2.bit.GPIO81 = 0;  //2;  // XA9
     GpioCtrlRegs.GPCMUX2.bit.GPIO82 = 0;  //2;  // XA10
     GpioCtrlRegs.GPCMUX2.bit.GPIO83 = 0;  //2;  // XA11
     GpioCtrlRegs.GPCMUX2.bit.GPIO84 = 2;  // XA12
     GpioCtrlRegs.GPCMUX2.bit.GPIO85 = 2;  // XA13
     GpioCtrlRegs.GPCMUX2.bit.GPIO86 = 2;  // XA14
     GpioCtrlRegs.GPCMUX2.bit.GPIO87 = 2;  // XA15
     GpioCtrlRegs.GPBMUX1.bit.GPIO39 = 2;  // XA16
     GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 2;  // XA17
     GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 2;  // XA18

     GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 2;  // XREADY
	 GpioCtrlRegs.GPBMUX1.bit.GPIO35 = 2;  // XRNW
     GpioCtrlRegs.GPBMUX1.bit.GPIO38 = 2;  // XWE0

     GpioCtrlRegs.GPBMUX1.bit.GPIO36 = 2;  // XZCS0
     GpioCtrlRegs.GPBMUX1.bit.GPIO37 = 2;  // XZCS7
     GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 2;  // XZCS6
     EDIS;
}

//===========================================================================
// No more.
//===========================================================================
