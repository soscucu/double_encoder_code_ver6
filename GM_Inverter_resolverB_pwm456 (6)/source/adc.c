#include "DSP28x_Project.h"
#include "MPLD.h"
#include "variable.h"

int ADCsetVal;

// AD
Uint16 ADCH[10] = {0. , 0. , 0. , 0. , 0. , 0. , 0. , 0., 0., 0.};
float ScaleAin[10] = {
		3.3/4096./3.3*5.1/30.*2000./3.,	// Ib
		3.3/4096./3.3*5.1/30.*2000./3.,  // Ia
		3.3/4096./2./9.0/10.*50010.,	// Vdc
		3.3/4096./3.3*5.1/30.*2000./3.,	// Ic
		0.,
		0.,	// IV
		0.,	// tempPCB
		0.,	// Vbc
		0.,
		0.,	// IW
		0.	// tempCON
};
float OffsetAin[10] = {0. , 0. , 0. , 0. , 0. , 0. , 0. , 0., 0., 0.};

void InitAdc(void)
{
	ADCsetVal= (int)( 1.65 / 2.5*(float)(0x03FF));	// make Reference voltage 1.65V(1.596V)

	AD_RD0 = 0x101;
	AD_RD0 = ADCsetVal;
	
	AD_RD1 = 0x101;
	AD_RD1 = ADCsetVal;

	AD_RD2 = 0x101; 
	AD_RD2 = ADCsetVal;
}

void ADC_Process(void)
{
	AD_RD0 = 0x100;
	AD_RD1 = 0x100;
//	AD_RD2 = 0x100;


	delaycc(0.005e-6);

	//	conversion start
	ADC0_SOC_START;
	delaycc(0.005e-6);
	ADC0_SOC_END;

	ADC1_SOC_START;
	delaycc(0.005e-6);
	ADC1_SOC_END;

//	ADC2_SOC_START;
//	delaycc(0.005e-6);
//	ADC2_SOC_END;


	while(ADC0_BUSY);
	ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD0_CHA0 : Ib_sen
	ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD0_CHB0 : Ia_sen

	while(ADC1_BUSY);
	ADCH[2] = (AD_RD1 - 0x0800) & 0x0FFF;			//		AD1_CHA0 : Vdc
	ADCH[3] = (AD_RD1 - 0x0800) & 0x0FFF;			//		AD1_CHB0 : Ic_sen

//	while(ADC2_BUSY);
//	ADCH[4] = (AD_RD2 - 0x0800) & 0x0FFF;			//		AD2_CHA0 : Temp_sen	(temperture)
//	ADCH[5] = (AD_RD2 - 0x0800) & 0x0FFF;			//		AD2_CHB0 : Idc

}

//===========================================================================
// End of file.
//===========================================================================
