/******************************************************************************
    FAULT.C
******************************************************************************/
#include "DSP28x_Project.h"
#include "MPLD.h"
#include "da.h"
#include "variable.h"
#include "fault.h"
#define ON 1
#define OFF 0

int faultDispMode = 0, faultData = 0;
int fault_done = 0;
long Flag_Analog_fault=0, Flag_IPM1_fault=0, Flag_IPM2_fault=0, Flag_SW_fault=0;
int faulttest = 0;
int flta=0;
int abcd=0, abcde=0;

FAULT_REG Fault;
void InitFault(void);
void InitFaultGpio(void);
interrupt void fault(void);

extern interrupt void eqep2_offset(void);
extern interrupt void eqep1_offset(void);
extern interrupt void offset(void);

void InitPIE(void)
{
	Fault.HW.all = 0x0;
	Fault.SW.all = 0x0;

	EALLOW;

	PieCtrlRegs.PIECTRL.bit.ENPIE = 1; // enable PIE

//	XIntruptRegs.XINT1CR.all = 0x01; // XINTx Control register Enable external interrupt
	XIntruptRegs.XINT2CR.all = 0x01; // Enable external interrupt
	XIntruptRegs.XINT3CR.all = 0x01; // Enable external interrupt

//	GpioIntRegs.GPIOXINT1SEL.bit.GPIOSEL = 15 ; //nFault_AS(Arm Short)
	GpioIntRegs.GPIOXINT2SEL.bit.GPIOSEL = 27 ; //current fault
	GpioIntRegs.GPIOXINT3SEL.bit.GPIOSEL = 54 ; //Vdc fault




	PieVectTable.EPWM1_TZINT = &fault;
	PieCtrlRegs.PIEIER2.bit.INTx1 = 1;

	PieVectTable.EPWM2_TZINT = &fault;
	PieCtrlRegs.PIEIER2.bit.INTx2 = 1;

	PieVectTable.EPWM3_TZINT = &fault;
	PieCtrlRegs.PIEIER2.bit.INTx3 = 1;

	//	PieVectTable.XINT1 = &fault;
	//	PieCtrlRegs.PIEIER1.bit.INTx4 = 1;

	PieVectTable.XINT2 = &fault;
	PieCtrlRegs.PIEIER1.bit.INTx5 = 1;

	PieVectTable.XINT3 = &fault;
	PieCtrlRegs.PIEIER12.bit.INTx1 = 1;

	PieVectTable.EPWM1_INT = &offset;
	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;

	PieVectTable.EQEP1_INT = &eqep1_offset;
	PieCtrlRegs.PIEIER5.bit.INTx1 = 1;

	PieVectTable.EQEP2_INT = &eqep2_offset;
	PieCtrlRegs.PIEIER5.bit.INTx2 = 1;

	IER |= M_INT1|M_INT2|M_INT3|M_INT5|M_INT9|M_INT12; // XINT12 | TZ | PWM(offset,cc) | SCI | EQep|XINT3

	EDIS;
	EINT;
	ERTM;
}

interrupt void fault(void)
{

	flta++;
//	RELAY_OFF;
//	PieCtrlRegs.PIECTRL.bit.ENPIE = 0;
//	DINT;

	if (GpioDataRegs.GPADAT.bit.GPIO27 == 0)	{ Fault.HW.bit.Current = 1;}

	if (GpioDataRegs.GPBDAT.bit.GPIO54 == 0)	{ Fault.HW.bit.Vdc = 1;}


//    if (GpioDataRegs.GPBDAT.bit.GPIO53 == 0)     {}
	FaultProcess();

//	fault_Vdc = Vdc;
//	fault_Vn = Vn;
//	fault_Ias1 = Ias1;
//	fault_Ibs1 = Ibs1;
//	fault_Ics1 = Ics1;
//	fault_Ias2 = Ias2;
//	fault_Ibs2 = Ibs2;
//	fault_Ics2 = Ics2;


	IER &= 0x0000;

	EALLOW;


	if (EPwm1Regs.TZFLG.bit.INT == 1)
	{
		Fault.HW.bit.Arm_short = 1;

		EPwm1Regs.TZCLR.bit.CBC = 1;
		EPwm1Regs.TZCLR.bit.INT = 1;

		EPwm2Regs.TZCLR.bit.CBC = 1;
		EPwm2Regs.TZCLR.bit.INT = 1;

		EPwm3Regs.TZCLR.bit.CBC = 1;
		EPwm3Regs.TZCLR.bit.INT = 1;

		EPwm4Regs.TZCLR.bit.CBC = 1;
		EPwm4Regs.TZCLR.bit.INT = 1;

		EPwm5Regs.TZCLR.bit.CBC = 1;
		EPwm5Regs.TZCLR.bit.INT = 1;

		EPwm6Regs.TZCLR.bit.CBC = 1;
	 	EPwm6Regs.TZCLR.bit.INT = 1;

		EPwm7Regs.TZCLR.bit.CBC = 1;
		EPwm7Regs.TZCLR.bit.INT = 1;

		EPwm8Regs.TZCLR.bit.CBC = 1;
		EPwm8Regs.TZCLR.bit.INT = 1;

		EPwm9Regs.TZCLR.bit.CBC = 1;
	 	EPwm9Regs.TZCLR.bit.INT = 1;
	}



//	GpioDataRegs.GPBCLEAR.bit.GPIO52 = 1;	// To cut-off relay

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;    // XINT1,2 flag clear
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;    // TZ flag clear
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;   // XINT3,4,5,6,7 flag clear

	IER = M_INT1|M_INT2|M_INT3|M_INT5|M_INT9|M_INT12;			// HW fault || TZ int || CC || EQEP|| EasyDSP || XINT 3,4,5,6,7
	EDIS;

	EINT;   // Enable Global interrupt INTM

}

void FaultProcess(void)
{

	SW1.CC = OFF;
	SW1.PC = OFF;
	SW1.SC = OFF;
	SW1.Align = OFF;

/*	EPwm1Regs.AQSFRC.bit.OTSFA = 1;
	EPwm1Regs.AQSFRC.bit.OTSFB = 1;
	EPwm2Regs.AQSFRC.bit.OTSFA = 1;
	EPwm2Regs.AQSFRC.bit.OTSFB = 1;
	EPwm3Regs.AQSFRC.bit.OTSFA = 1;
	EPwm3Regs.AQSFRC.bit.OTSFB = 1;
	EPwm4Regs.AQSFRC.bit.OTSFA = 1;
	EPwm4Regs.AQSFRC.bit.OTSFB = 1;
	EPwm5Regs.AQSFRC.bit.OTSFA = 1;
	EPwm5Regs.AQSFRC.bit.OTSFB = 1;
	EPwm6Regs.AQSFRC.bit.OTSFA = 1;
	EPwm6Regs.AQSFRC.bit.OTSFB = 1;

	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm4Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm5Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm5Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm6Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm6Regs.AQCTLA.bit.CAD = AQ_CLEAR;


	EPwm1Regs.CMPA.half.CMPA = 0;
	EPwm2Regs.CMPA.half.CMPA = 0;
	EPwm3Regs.CMPA.half.CMPA = 0;
	EPwm4Regs.CMPA.half.CMPA = 0;
	EPwm5Regs.CMPA.half.CMPA = 0;
	EPwm6Regs.CMPA.half.CMPA = 0;
*/
	Switch_DSP_Off();




}


