#include "DSP28x_Project.h"
#include "variable.h"
#include "MPLD.h"

long *da[4] = {0 , 0 , 0 , 0};
int	 da_type[4] = {0 , 0 , 0 , 0} , da_data[4] ={0 ,0 ,0 ,0};
float da_scale[4] = {5., 5., 5., 5.}, da_mid[4] = {0., 0., 0., 0.}, da_temp[4] = {0., 0., 0., 0.};
int DacOffset = 0;

void InitSerialDac(void)
{
	InitSpiGpio();

	SpidRegs.SPICCR.bit.SPISWRESET = 0;

	SpidRegs.SPICCR.all = 0x0047;//0x03;	//-> char length 8bit		// Clock polarity = 1
											// SPI Baud Rate = LSPCLK/(SPIBRR + 1) for SPIBRR = 3~127
											// LSPCLK = SYSCLKOUT/2 = 150MHz
	SpidRegs.SPICTL.all = 0x000E;		// Master mode, Enable transmission, Phase = 0
	SpidRegs.SPISTS.all = 0x0000;
	//SpidRegs.SPIBRR = 0x0003;//5	// LSPCLK/(3 + 1) = 37.5MHz
    SpidRegs.SPIBRR = 0x0005;//5	// LSPCLK/(3 + 1) = 37.5MHz

	SpidRegs.SPIPRI.bit.FREE = 1;

	//추가
	SpidRegs.SPIFFTX.all=0xC008;      // Enable FIFO's, set TX FIFO level to 8
	SpidRegs.SPIFFRX.all=0x001F;      // Set RX FIFO level to 8
	SpidRegs.SPIFFCT.all=0x00;
	SpidRegs.SPIPRI.all=0x0010;

	SpidRegs.SPICCR.bit.SPISWRESET = 1;

	//추가
	SpidRegs.SPIFFTX.bit.TXFIFO=1;
	SpidRegs.SPIFFRX.bit.RXFIFORESET=1;
	// SPI D 만 설정(DAC)


	GpioDataRegs.GPBSET.bit.GPIO51=1;  //ncs  low active
	while(SpidRegs.SPIFFTX.bit.TXFFST);
	asm("	nop");
	asm("	nop");
	asm("	nop");
	GpioDataRegs.GPBCLEAR.bit.GPIO51=1;  //ncs  low active

	SpidRegs.SPITXBUF = 0x0100;  //  WRITE AND loa4d data for CH0
	SpidRegs.SPITXBUF = 0x1000;
	SpidRegs.SPITXBUF = 0x0000;

	GpioDataRegs.GPBSET.bit.GPIO51=1;  //ncs  low active
	while(SpidRegs.SPIFFTX.bit.TXFFST);
	asm("	nop");
	GpioDataRegs.GPBCLEAR.bit.GPIO51=1;  //ncs  low active

}

void SerialDacOut(void)
{
	da_temp[0] = da_type[0] ? (float)(*da[0]) : *(float *)(da[0]);
	da_data[0] = ((int)(DacOffset - da_scale[0] * (da_temp[0] - da_mid[0])));

	SpidRegs.SPITXBUF =0x1000;  //  WRITE AND loa4d data for CH0
	SpidRegs.SPITXBUF =(int)((da_data[0]<<4)&0xFF00);
	SpidRegs.SPITXBUF =(int)((da_data[0]<<12)&0xFF00);
	//delaycc(1e-6);

	delaycc(1.46e-6);

	da_temp[1] = da_type[1] ? (float)(*da[1]) : *(float *)(da[1]);
	da_data[1] = ((int)(DacOffset - da_scale[1] * (da_temp[1] - da_mid[1])));

	SpidRegs.SPITXBUF =0x1200;  //  WRITE AND loa4d data for CH0
	SpidRegs.SPITXBUF =(int)((da_data[1]<<4)&0xFF00);
	SpidRegs.SPITXBUF =(int)((da_data[1]<<12)&0xFF00);
    //delaycc(1e-6);

	delaycc(1.46e-6);

	da_temp[2] = da_type[2] ? (float)(*da[2]) : *(float *)(da[2]);
 	da_data[2] = ((int)(DacOffset - da_scale[2] * (da_temp[2] - da_mid[2])));

	SpidRegs.SPITXBUF =0x1400;  //  WRITE AND loa4d data for CH0
	SpidRegs.SPITXBUF =(int)((da_data[2]<<4)&0xFF00);
	SpidRegs.SPITXBUF =(int)((da_data[2]<<12)&0xFF00);

	//delaycc(0.8e-6);
    delaycc(1.46e-6);

	da_temp[3] = da_type[3] ? (float)(*da[3]) : *(float *)(da[3]);
	da_data[3] = ((int)(DacOffset - da_scale[3] * (da_temp[3] - da_mid[3])));
//	da_data[3] = (int)((da_temp[3] - da_mid[3]) * da_scale[3]) + 0x800;

	SpidRegs.SPITXBUF =0x1600;  //  WRITE AND loa4d data for CH0
	SpidRegs.SPITXBUF =(int)((da_data[3]<<4)&0xFF00);
	SpidRegs.SPITXBUF =(int)((da_data[3]<<12)&0xFF00);

}


