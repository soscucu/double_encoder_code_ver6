#include <math.h>
#include "DSP2834x_Device.h"
#include "DSP28x_Project.h"
#include "MPLD.h"
#include "da.h"
#include "cc.h"
#include "adc.h"
#include "comp.h"
#include "variable.h"
#include "filter.h"
#include "fault.h"
#define ON 1
#define OFF 0



interrupt void cc(void)
{
	ccTimer2start = ReadCpuTimer2Counter();

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IERBackupCC = IER;
	IER = M_INT1|M_INT2|M_INT3|M_INT5|M_INT9|M_INT12;		// XINT12 | TZ | PWM(offset,cc) | SCI | EQep|XINT3

	EINT;   /* Enable Global interrupt INTM */

	delaycc(Tdelay);	/* 측정시간 확보를 위해 Tdelay만큼 측정시간을 미룸 */

	ADC_Process();

	Inv1.Ib	= ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0]*((float)offsetMaxCnt);// + Scalecur ;
	Inv1.Ia	= ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1]*((float)offsetMaxCnt);// + Scalecur ;
	Inv1.Ic	= ((float) ADCH[3]-OffsetAin[3]) * ScaleAin[3]*((float)offsetMaxCnt);// + Scalecur ;

	Inv1.Vdc = ((float) ADCH[2]) * ScaleAin[2]*((float)offsetMaxCnt);

	Thetam_enc1 = EncoderAngleGenerator(1);
	Thetam_enc2 = EncoderAngleGenerator(2);

	if(SW1.CC == ON){
		Switch_DSP_On();

		EPwm1Regs.CMPA.half.CMPA = test_pwm_duty4 * maxCount_ePWM;
		EPwm2Regs.CMPA.half.CMPA = test_pwm_duty5 * maxCount_ePWM;
		EPwm3Regs.CMPA.half.CMPA = test_pwm_duty6 * maxCount_ePWM;

	}
	else{
		test_pwm_duty4 = 0.;
		test_pwm_duty5 = 0.;
		test_pwm_duty6 = 0.;

		Switch_DSP_Off();
		SW1.Buffer = OFF;
	}


	SerialDacOut();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
		@brief		PWM out
	 */
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	EPwm1Regs.ETCLR.bit.INT = 1;	 /*	for Clear Interrupt Flag at PRD */
	EPwm2Regs.ETCLR.bit.INT = 1;	 /* for Clear Interrupt Flag at Zero -->> Double sampling */

	Time_CC = (float)(ccTimer2start - ReadCpuTimer2Counter())*SYS_CLK_PRD;	/*	calculating CC loop time */

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupCC;			/* HW fault & TINT0 || TZ || CC || EasyDSP */



}




