/*****************************************************************************
    OFFSET.C
******************************************************************************/
#include <math.h>
#include "DSP28x_Project.h"
#include "MPLD.h"
#include "da.h"
#include "cc.h"
#include "comp.h"
#include "variable.h"
#include "adc.h"
#include "offset.h"
#include "fault.h"

Uint16 uOffsetLoopCnt = 0, uOffsetMaxCnt = (1<<14);	// power of 2, maximum = 2^14
extern interrupt void cc(void);	/* for CC*/
int offset_v = 0;
interrupt void offset(void)
{
	int i=0;


	// Conversion Delay
	SerialDacOut();	// about 1us

	offsetLoopCnt++;
	if (offset_v == 0 )
	{
		if (offsetLoopCnt >= 100 )
		{
			offset_v = 1;
			offsetLoopCnt = 0 ;
		}
	}

	else
	{

		delaycc(Tdelay);	/* 측정시간 확보를 위해 Tdelay만큼 측정시간을 미룸 */

		AD_RD0= 0x100;
		AD_RD1= 0x100;
		AD_RD2= 0x100;

  		delaycc(0.005e-6);

		ADC0_SOC_START;
		ADC1_SOC_START;
		ADC2_SOC_START;

		delaycc(0.005e-6);
		ADC0_SOC_END;
		ADC1_SOC_END;
		ADC2_SOC_END;

		while(ADC0_BUSY);
		OffsetAin[0] += (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT
		OffsetAin[1] += (AD_RD0 - 0x0800) & 0x0FFF;

		while(ADC1_BUSY);

		OffsetAin[2] += (AD_RD1) & 0x0FFF;		//		AD_VOUT
		OffsetAin[3] += (AD_RD1 - 0x0800) & 0x0FFF;

	}

// ADC Offset setting

	// Offset value calculation
	if(offsetLoopCnt >= offsetMaxCnt)
	{
		// Calc. offset
		for (i=0; i<16; i++)
		{
			OffsetAin[i] = OffsetAin[i] / (float)offsetMaxCnt;
		}

		EALLOW;
		PieVectTable.EPWM1_INT = &cc;
		PieVectTable.EPWM2_INT = &cc;
		EDIS;
	}

	EPwm1Regs.ETCLR.bit.INT = 1; 					//	for interrupt at zero
	EPwm2Regs.ETCLR.bit.INT = 1;					//  for interrupt at PRD		-->> Double sampling
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;

}
/*------------------------------------------------------------------*/
/*         End of OFFSET.C											*/
/*------------------------------------------------------------------*/
