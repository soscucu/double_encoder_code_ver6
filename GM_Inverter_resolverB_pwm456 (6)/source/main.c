#include "DSP28x_Project.h"
#include "easy2834x_sci_v7.4.h"
#include "MPLD.h"
#include "adc.h"
#include "da.h"
#include "variable.h"
#include "fault.h"
#include "offset.h"
#include "Init_Func.h"
#include "comp.h"
#define ON  1
#define OFF 0
Uint32 loopCnt = 0;
Uint16 Controller_reset = 0;



void main(void)
{	
	InitSysCtrl();
	InitGpio();
	DINT;

	///interrupt setting
	InitPieCtrl();

	IER = 0x0000;
	IFR = 0x0000;

	InitPieVectTable();

	InitPIE(); // TZINT, XINT, EQEPINT register
    InitTzGpio();

	///////////////////////////////////////////////

	initEnc(&encoder, 2500, 4);
	InitEQep();
	InitEQepGpio();

	InitCpuTimers();
	ConfigCpuTimer(&CpuTimer0, 300, SYS_CLK_PRD);
	CpuTimer0Regs.TCR.all = 0x4000;


	XintfInit();

	InitEPwm(); //1,2,3,4,5,6 configuration

	InitAdc();
	InitSerialDac();

	easyDSP_SCI_Init();
	easyDSP_SPI_Flashrom_Init();

	Tsamp = 2. * maxCount_ePWM * SYS_CLK_PRD; // 1* for double sampling 2.* for single sampling


	InitParameter();
	InitController();
//	SwitchOff();
	Switch_DSP_Off();

	InitFlag();
	InitSW();
	InitUIGpio();

	while(1)
	{
		loopCnt++;
		UpdatePara();

		if(SW1.CC == OFF && SW1.Controller_param_set == ON) InitController();

		if(SW1.Relay)	Relay_ON;
		else Relay_OFF;

		if(SW1.Buffer) {
			Buffer_ON;
			LED2_ON;
		}
		else {
			Buffer_OFF;
			LED2_OFF;
		}
		if(SW1.CC) LED3_ON;
		else LED3_OFF;

		if(SW1.SC) LED4_ON;
		else LED4_OFF;

		if(SW1.PC) LED5_ON;
		else LED5_OFF;

		if(SW1.Double_sampling) LED6_ON;
		else LED6_OFF;


		if(GpioDataRegs.GPADAT.bit.GPIO11) Flag.Manual_switch = ON;
		else {
			Flag.Manual_switch = OFF;
			SW1.CC = OFF;
		}
		if(GpioDataRegs.GPBDAT.bit.GPIO60) Flag.Relay = ON;
		else 							   Flag.Relay = OFF;
		if(GpioDataRegs.GPBDAT.bit.GPIO56) Flag.Buffer = OFF;
		else 							   Flag.Buffer = ON;
		if(GpioDataRegs.GPBDAT.bit.GPIO62) Flag.LED2 = ON;
		else 							   Flag.LED2 = OFF;
		if(GpioDataRegs.GPBDAT.bit.GPIO32) Flag.LED3 = ON;
		else 							   Flag.LED3 = OFF;
		if(GpioDataRegs.GPBDAT.bit.GPIO49) Flag.LED4 = ON;
		else 							   Flag.LED4 = OFF;
		if(GpioDataRegs.GPBDAT.bit.GPIO52) Flag.LED5 = ON;
		else 							   Flag.LED5 = OFF;
		if(GpioDataRegs.GPBDAT.bit.GPIO59) Flag.LED6 = ON;
		else 							   Flag.LED6 = OFF;

	}
}


void UpdatePara(){

}

//===========================================================================
// No more.
//===========================================================================
