// TI File $Revision: /main/1 $
// Checkin $Date: February 1, 2008   09:59:35 $
//###########################################################################
//
// FILE:	DSP2834x_Gpio.c
//
// TITLE:	DSP2834x General Purpose I/O Initialization & Support Functions.
//
//###########################################################################
// $TI Release: DSP2834x C/C++ Header Files V1.10 $
// $Release Date: July 27, 2009 $
//###########################################################################

#include "DSP2834x_Device.h"     // DSP2834x Headerfile Include File
#include "DSP2834x_Examples.h"   // DSP2834x Examples Include File

//---------------------------------------------------------------------------
// InitGpio: 
//---------------------------------------------------------------------------
// This function initializes the Gpio to a known (default) state.
//
// For more details on configuring GPIO's as peripheral functions,
// refer to the individual peripheral examples and/or GPIO setup example. 

void InitGpio(void)
{
   EALLOW;

   // Each GPIO pin can be:
   // a) a GPIO input/output
   // b) peripheral function 1
   // c) peripheral function 2
   // d) peripheral function 3
   // By default, all are GPIO Inputs


	GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1;	//GPIO0 - ePWM1A
//	GpioCtrlRegs.GPADIR.bit.GPIO0 = 1;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO0 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO0 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 1;	//GPIO1 - ePWM1B
//	GpioCtrlRegs.GPADIR.bit.GPIO1 = 1;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO1 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO1 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 1;	//GPIO2 - ePWM2A
//	GpioCtrlRegs.GPADIR.bit.GPIO2 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO2 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO2 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 1;	//GPIO3 - ePWM2B
//	GpioCtrlRegs.GPADIR.bit.GPIO3 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO3 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO3 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 1;	//GPIO4 - ePWM3A
//	GpioCtrlRegs.GPADIR.bit.GPIO4 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO4 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO4 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO5 = 1;	//GPIO5 - ePWM3B
//	GpioCtrlRegs.GPADIR.bit.GPIO5 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO5 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO5 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0;	//GPIO6 - ePWM4A
//	GpioCtrlRegs.GPADIR.bit.GPIO6 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO6 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO6 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0;	//GPIO7 - ePWM4B
//	GpioCtrlRegs.GPADIR.bit.GPIO7 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO7 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO7 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 0;	//GPIO8 - ePWM5A
//	GpioCtrlRegs.GPADIR.bit.GPIO8 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO8 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO8 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 0;	//GPIO9 - ePWM5B
//	GpioCtrlRegs.GPADIR.bit.GPIO9 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO9 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO9 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 0;	//GPIO10 - ePWM6A
//	GpioCtrlRegs.GPADIR.bit.GPIO10 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO10 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO10 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO11 = 0;	//GPIO11 - ePWM6B
//	GpioCtrlRegs.GPADIR.bit.GPIO11 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO11 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO11 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 1;	//GPIO12 - nTZ1
	GpioCtrlRegs.GPADIR.bit.GPIO12 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO12 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO12 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO13 = 1;	//GPIO13 - nTZ2
	GpioCtrlRegs.GPADIR.bit.GPIO13 = 0;	//Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO13 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO13 = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO14 = 0;	//GPIO14 - n_epwm1
	GpioDataRegs.GPASET.bit.GPIO14 = 1;
	GpioCtrlRegs.GPADIR.bit.GPIO14 = 1;
	GpioCtrlRegs.GPAQSEL1.bit.GPIO14 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO14 = 0;

////NC (For [SNU-LG] 1 Micom 2 Motors)
	GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 1;	//GPIO15 - TZint
	GpioCtrlRegs.GPADIR.bit.GPIO15 = 0;	    //Input
	GpioCtrlRegs.GPAQSEL1.bit.GPIO15 = 0;   //immediate input
	GpioCtrlRegs.GPAPUD.bit.GPIO15 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 1;	//GPIO16 - SPISIMOA
//	GpioCtrlRegs.GPADIR.bit.GPIO16 = 1;	//Input Output Bidirection
	GpioCtrlRegs.GPAQSEL2.bit.GPIO16 = 3;
	GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 1;	//GPIO17 - SPISOMIA
//	GpioCtrlRegs.GPADIR.bit.GPIO17 = 1;	//Input Output Bidirection
	GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 3;
	GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO18 = 1;	//GPIO18 - SPICLKA
//	GpioCtrlRegs.GPADIR.bit.GPIO18 = 1;	//Input Output Bidirection
	GpioCtrlRegs.GPAQSEL2.bit.GPIO18 = 3;
	GpioCtrlRegs.GPAPUD.bit.GPIO18 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 1;	//GPIO19 - nSPISTEA
//	GpioCtrlRegs.GPADIR.bit.GPIO19 = 1;	//Input Output Bidirection
	GpioCtrlRegs.GPAQSEL2.bit.GPIO19 = 3;
	GpioCtrlRegs.GPAPUD.bit.GPIO19 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO20 = 1;	//GPIO20 - ENC1_A
//	GpioCtrlRegs.GPADIR.bit.GPIO20 = 0;	//Input Output Bidirection
	GpioCtrlRegs.GPAQSEL2.bit.GPIO20 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO20 = 0;


	GpioCtrlRegs.GPAMUX2.bit.GPIO21 = 1;	//GPIO21 - ENC1_B
//	GpioCtrlRegs.GPADIR.bit.GPIO21 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO21 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO21 = 0;

////NC (For [SNU-LG] 1 Micom 2 Motors)
	GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 0;	//GPIO22 - UARTTX
//	GpioCtrlRegs.GPADIR.bit.GPIO22 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO22 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO22 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 1;	//GPIO23 - ENC1_Z
//	GpioCtrlRegs.GPADIR.bit.GPIO23 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO23 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO23 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO24 = 2;	//GPIO24 - ENC2_A
//	GpioCtrlRegs.GPADIR.bit.GPIO24 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO24 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO24 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 2;	//GPIO25 - ENC2_B
//	GpioCtrlRegs.GPADIR.bit.GPIO25 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO25 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO25 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 2;	//GPIO26 - ENC2_Z
//	GpioCtrlRegs.GPADIR.bit.GPIO26 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO26 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO26 = 0;

////NC (For [SNU-LG] 1 Micom 2 Motors)
	GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 0;	//GPIO27 - nINT1
	GpioCtrlRegs.GPADIR.bit.GPIO27 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO27 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO27 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 1;	//GPIO28 - SCIRXDA
	GpioCtrlRegs.GPADIR.bit.GPIO28 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO28 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO28 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 1;	//GPIO29 - SCITXDA
	GpioCtrlRegs.GPADIR.bit.GPIO29 = 1;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO29 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO29 = 0;
	/*
	GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 2;	//GPIO30 - A18
	GpioCtrlRegs.GPADIR.bit.GPIO30 = 1;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO30 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO30 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 2;	//GPIO31 - A17
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 1;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO31 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO31 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0;	//GPIO32 - DOUT2	
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1;	
	GpioCtrlRegs.GPBQSEL1.bit.GPIO32 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO32 = 0;
	*/
	GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 0;	//GPIO30 - A18	 //dmc
	GpioCtrlRegs.GPADIR.bit.GPIO30 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO30 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO30 = 0;

	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 0;	//GPIO31 - A17	 /dmc
	GpioCtrlRegs.GPADIR.bit.GPIO31 = 0;
	GpioCtrlRegs.GPAQSEL2.bit.GPIO31 = 0;
	GpioCtrlRegs.GPAPUD.bit.GPIO31 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0;	//GPIO32 - DOUT2  /dmc
	GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO32 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO32 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO33 = 0;	//GPIO33 - nFLTVDC
	GpioCtrlRegs.GPBDIR.bit.GPIO33 = 0;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO33 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO33 = 0;

////NC (For [SNU-LG] 1 Micom 2 Motors)
	GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 2;	//GPIO34 - XREADY
	GpioCtrlRegs.GPBDIR.bit.GPIO34 = 0;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO34 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO34 = 0;

////NC (For [SNU-LG] 1 Micom 2 Motors)
	GpioCtrlRegs.GPBMUX1.bit.GPIO35 = 2;	//GPIO35 - XRnW
	GpioCtrlRegs.GPBDIR.bit.GPIO35 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO35 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO35 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO36 = 2;	//GPIO36 - XZCS0
	GpioCtrlRegs.GPBDIR.bit.GPIO36 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO36 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO36 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO37 = 2;	//GPIO37 - XZCS7
	GpioCtrlRegs.GPBDIR.bit.GPIO37 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO37 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO37 = 0;

////NC (For [SNU-LG] 1 Micom 2 Motors)
	GpioCtrlRegs.GPBMUX1.bit.GPIO38 = 2;	//GPIO38 - nWR
	GpioCtrlRegs.GPBDIR.bit.GPIO38 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO38 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO38 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO39 = 2;	//GPIO39 - XA16
	GpioCtrlRegs.GPBDIR.bit.GPIO39 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO39 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO39 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO40 = 2;	//GPIO40 - XA0
	GpioCtrlRegs.GPBDIR.bit.GPIO40 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO40 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO40 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 2;	//GPIO41 - XA1
	GpioCtrlRegs.GPBDIR.bit.GPIO41 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO41 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO41 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO42 = 2;	//GPIO42 - XA2
	GpioCtrlRegs.GPBDIR.bit.GPIO42 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO42 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO42 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 2;	//GPIO43 - XA3
	GpioCtrlRegs.GPBDIR.bit.GPIO43 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO43 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO43 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 2;	//GPIO44 - XA4
	GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO44 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO44 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO45 = 2;	//GPIO45 - XA5
	GpioCtrlRegs.GPBDIR.bit.GPIO45 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO45 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO45 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO46 = 0;	//GPIO46 - ADCSOC0
	GpioCtrlRegs.GPBDIR.bit.GPIO46 = 1;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO46 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO46 = 0;

	GpioCtrlRegs.GPBMUX1.bit.GPIO47 = 0;	//GPIO47 - ADCBZ0
	GpioCtrlRegs.GPBDIR.bit.GPIO47 = 0;
	GpioCtrlRegs.GPBQSEL1.bit.GPIO47 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO47 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO48 = 3;	//GPIO48 - SPISIMOD
	GpioCtrlRegs.GPBDIR.bit.GPIO48 = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO48 = 3;
	GpioCtrlRegs.GPBPUD.bit.GPIO48 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO49 = 0;	//GPIO49 - DOUT1
	GpioCtrlRegs.GPBDIR.bit.GPIO49 = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO49 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO49 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO50 = 3;	//GPIO50 - SPICLKD
//	GpioCtrlRegs.GPBDIR.bit.GPIO50 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO50 = 3;
	GpioCtrlRegs.GPBPUD.bit.GPIO50 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO51 = 0;	//GPIO51 - nSPISTED
	GpioCtrlRegs.GPBDIR.bit.GPIO51 = 1;		//nSYNC�� GRIO�� ����
	GpioCtrlRegs.GPBQSEL2.bit.GPIO51 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO51 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO52 = 0;	//GPIO52 - DOUT0
	GpioCtrlRegs.GPBDIR.bit.GPIO52 = 1;	
	GpioCtrlRegs.GPBQSEL2.bit.GPIO52 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO52 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO53 = 0;	//GPIO53 - nFLTI0
	GpioCtrlRegs.GPBDIR.bit.GPIO53 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO53 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO53 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO54 = 0;	//GPIO54 - nFLTI1
	GpioCtrlRegs.GPBDIR.bit.GPIO54 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO54 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO54 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO55 = 0;	//GPIO55 - DIN0
	GpioCtrlRegs.GPBDIR.bit.GPIO55 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO55 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO55 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO56 = 0;	//GPIO56 - n_epwm2
	GpioDataRegs.GPBSET.bit.GPIO56 = 1;
	GpioCtrlRegs.GPBDIR.bit.GPIO56 = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO56 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO56 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO57 = 0;	//GPIO57 - DIN1
	GpioCtrlRegs.GPBDIR.bit.GPIO57 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO57 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO57 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO58 = 0;	//GPIO58 - Hall1_b
	GpioCtrlRegs.GPBDIR.bit.GPIO58 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO58 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO58 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO59 = 0;	//GPIO59 - Hall2_a
	GpioCtrlRegs.GPBDIR.bit.GPIO59 = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO59 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO59 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO60 = 0;	//GPIO60 - Hall2_b
	GpioCtrlRegs.GPBDIR.bit.GPIO60 = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO60 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO60 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO61 = 0;	//GPIO61 - Hall2_a
	GpioCtrlRegs.GPBDIR.bit.GPIO61 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO61 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO61 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO62 = 0;	//GPIO62 - nc
	GpioCtrlRegs.GPBDIR.bit.GPIO62 = 1;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO62 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO62 = 0;

	GpioCtrlRegs.GPBMUX2.bit.GPIO63 = 0;	//GPIO63 - nc
	GpioCtrlRegs.GPBDIR.bit.GPIO63 = 0;
	GpioCtrlRegs.GPBQSEL2.bit.GPIO63 = 0;
	GpioCtrlRegs.GPBPUD.bit.GPIO63 = 0;

//////////////////////////////////////////////////////////////////////////////////////

	GpioCtrlRegs.GPCMUX1.bit.GPIO64 = 3;	//GPIO64 - D15
	GpioCtrlRegs.GPCDIR.bit.GPIO64 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO64 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO65 = 3;	//GPIO64 - D14
	GpioCtrlRegs.GPCDIR.bit.GPIO65 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO65 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO66 = 3;	//GPIO64 - D13
	GpioCtrlRegs.GPCDIR.bit.GPIO66 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO66 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO67 = 3;	//GPIO64 - D12
	GpioCtrlRegs.GPCDIR.bit.GPIO67 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO67 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO68 = 3;	//GPIO64 - D11
	GpioCtrlRegs.GPCDIR.bit.GPIO68 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO68 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO69 = 3;	//GPIO64 - D10
	GpioCtrlRegs.GPCDIR.bit.GPIO69 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO69 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO70 = 3;	//GPIO64 - D9
	GpioCtrlRegs.GPCDIR.bit.GPIO70 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO70 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO71 = 3;	//GPIO64 - D8
	GpioCtrlRegs.GPCDIR.bit.GPIO71 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO71 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO72 = 3;	//GPIO64 - D7
	GpioCtrlRegs.GPCDIR.bit.GPIO72 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO72 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO73 = 3;	//GPIO64 - D6
	GpioCtrlRegs.GPCDIR.bit.GPIO73 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO73 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO74 = 3;	//GPIO64 - D5
	GpioCtrlRegs.GPCDIR.bit.GPIO74 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO74 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO75 = 3;	//GPIO64 - D4
	GpioCtrlRegs.GPCDIR.bit.GPIO75 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO75 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO76 = 3;	//GPIO64 - D3
	GpioCtrlRegs.GPCDIR.bit.GPIO76 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO76 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO77 = 3;	//GPIO64 - D2
	GpioCtrlRegs.GPCDIR.bit.GPIO77 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO77 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO78 = 3;	//GPIO64 - D1
	GpioCtrlRegs.GPCDIR.bit.GPIO78 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO78 = 0;

	GpioCtrlRegs.GPCMUX1.bit.GPIO79 = 3;	//GPIO64 - D0
	GpioCtrlRegs.GPCDIR.bit.GPIO79 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO79 = 0;


//////////////////////////////////////////////////////////////////////////////////////
	GpioCtrlRegs.GPCMUX1.all=0xFFFFFFFF;
//	GpioCtrlRegs.GPCDIR.all=0x
//	GpioCtrlRegs.GPCMUX2.all=0x0000FFFF;
//	GpioCtrlRegs.GPCDIR.all=0x00000000;
//	GpioCtrlRegs.GPCPUD.all=0x00000000;

	GpioCtrlRegs.GPCMUX2.bit.GPIO80 = 0;	//GPIO80 - ADCSOC2
	GpioCtrlRegs.GPCDIR.bit.GPIO80 = 1;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO80 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO80 = 0;

	GpioCtrlRegs.GPCMUX2.bit.GPIO81 = 0;	//GPIO81 - ADCSOC1
	GpioCtrlRegs.GPCDIR.bit.GPIO81 = 1;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO81 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO81 = 0;

	GpioCtrlRegs.GPCMUX2.bit.GPIO82 = 0;	//GPIO82 - ADCBZ2
	GpioCtrlRegs.GPCDIR.bit.GPIO82 = 0;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO82 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO82 = 0;

	GpioCtrlRegs.GPCMUX2.bit.GPIO83 = 0;	//GPIO83 - ADCBZ1
	GpioCtrlRegs.GPCDIR.bit.GPIO83 = 0;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO83 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO83 = 0;

	GpioCtrlRegs.GPCMUX2.bit.GPIO84 = 3;	//GPIO84 - XA12
//	GpioCtrlRegs.GPCDIR.bit.GPIO84 = 0;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO84 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO84 = 0;

	GpioCtrlRegs.GPCMUX2.bit.GPIO85 = 3;	//GPIO85 - XA13
//	GpioCtrlRegs.GPCDIR.bit.GPIO85 = 0;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO85 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO85 = 0;

	GpioCtrlRegs.GPCMUX2.bit.GPIO86 = 3;	//GPIO84 - XA14
//	GpioCtrlRegs.GPCDIR.bit.GPIO86 = 0;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO86 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO86 = 0;

	GpioCtrlRegs.GPCMUX2.bit.GPIO87 = 3;	//GPIO87 - XA15
//	GpioCtrlRegs.GPCDIR.bit.GPIO87 = 0;
//	GpioCtrlRegs.GPCQSEL2.bit.GPIO87 = 0;
	GpioCtrlRegs.GPCPUD.bit.GPIO87 = 0;

  EDIS;
	EDIS;
}
	
//===========================================================================
// End of file.
//===========================================================================
